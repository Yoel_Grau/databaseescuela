import java.sql.Connection;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            Connection connection = DBConexion.getConnection();
            EscuelaManager manager = new EscuelaManager(connection);
            Scanner scanner = new Scanner(System.in);
            int opcion;

            do {
                System.out.println("\n*** Menú de administracion de la Escuela ***\n");
                System.out.println("1. Agregar Estudiante");
                System.out.println("2. Mostrar Estudiantes");
                System.out.println("3. Actualizar Estudiante");
                System.out.println("4. Eliminar Estudiante");
                System.out.println("5. Salir");
                System.out.print("Seleccione una opción: ");
                opcion = scanner.nextInt();
                scanner.nextLine();

                switch (opcion) {
                    case 1:
                        System.out.print("Ingrese nombre: ");
                        String nombre = scanner.nextLine();
                        System.out.print("Ingrese apellido: ");
                        String apellido = scanner.nextLine();
                        System.out.print("Ingrese email: ");
                        String email = scanner.nextLine();
                        manager.agregarEstudiante(nombre, apellido, email);
                        break;
                    case 2:
                        manager.MostrarEstudiantes();
                        break;
                    case 3:
                        System.out.print("Ingrese ID del estudiante a actualizar: ");
                        int idActualizar = scanner.nextInt();
                        scanner.nextLine();
                        System.out.print("Ingrese nuevo nombre: ");
                        String nuevoNombre = scanner.nextLine();
                        System.out.print("Ingrese nuevo apellido: ");
                        String nuevoApellido = scanner.nextLine();
                        System.out.print("Ingrese nuevo email: ");
                        String nuevoEmail = scanner.nextLine();
                        manager.actualizarEstudiante(idActualizar, nuevoNombre, nuevoApellido, nuevoEmail);
                        break;
                    case 4:
                        System.out.print("Ingrese ID del estudiante a eliminar: ");
                        int idEliminar = scanner.nextInt();
                        manager.eliminarEstudiante(idEliminar);
                        break;
                    case 5:
                        System.out.println("Bye.");
                        break;
                    default:
                        System.out.println("Error: La opcion no es correcta.");
                        break;
                }
            } while (opcion != 5);

        } catch (Exception e) {
            System.out.println("Error: No se pudo conectar a la base de datos: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
