import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EscuelaManager {

    private Connection connection;

    public EscuelaManager(Connection connection) {
        this.connection = connection;
    }

    public void agregarEstudiante(String nombre, String apellido, String email) {
        String sql = "INSERT INTO Estudiantes (Nombre, Apellido, Email) VALUES (?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, nombre);
            statement.setString(2, apellido);
            statement.setString(3, email);
            statement.executeUpdate();
            System.out.println("Estudiante agregado exitosamente.");
        } catch (SQLException e) {
            System.out.println("Error al agregar estudiante: " + e.getMessage());
        }
    }

    public void MostrarEstudiantes() {
        String sql = "SELECT * FROM Estudiantes";
        try (PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                System.out.println(resultSet.getInt("ID_Estudiante") + " - " +
                        resultSet.getString("Nombre") + " " +
                        resultSet.getString("Apellido") + " - " +
                        resultSet.getString("Email"));
            }
        } catch (SQLException e) {
            System.out.println("Error al mostrar los estudiantes: " + e.getMessage());
        }
    }

    public void actualizarEstudiante(int id, String nombre, String apellido, String email) {
        String sql = "UPDATE Estudiantes SET Nombre = ?, Apellido = ?, Email = ? WHERE ID_Estudiante = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, nombre);
            statement.setString(2, apellido);
            statement.setString(3, email);
            statement.setInt(4, id);
            int rowsAffected = statement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Estudiante actualizado exitosamente.");
            } else {
                System.out.println("No se encontró un estudiante con el ID ingresado.");
            }
        } catch (SQLException e) {
            System.out.println("Error al actualizar estudiante: " + e.getMessage());
        }
    }

    public void eliminarEstudiante(int id) {
        String sql = "DELETE FROM Estudiantes WHERE ID_Estudiante = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            int rowsAffected = statement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Estudiante eliminado exitosamente.");
            } else {
                System.out.println("No se encontró un estudiante con el ID ingresado.");
            }
        } catch (SQLException e) {
            System.out.println("Error al eliminar estudiante: " + e.getMessage());
        }
    }
}
