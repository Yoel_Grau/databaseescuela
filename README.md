# Codigo SQL de la base de datos Escuela

CREATE DATABASE Escuela;
SHOW DATABASES;

USE Escuela;

CREATE TABLE Estudiantes (
ID_Estudiante INT AUTO_INCREMENT PRIMARY KEY,
Nombre VARCHAR(20),
Apellido VARCHAR(20),
Email VARCHAR(60) UNIQUE
);

CREATE TABLE Profesores (
ID_Profesor INT AUTO_INCREMENT PRIMARY KEY,
Nombre VARCHAR(20),
Apellido VARCHAR(20),
Especialidad VARCHAR(60)
);

CREATE TABLE Clases (
ID_Clase INT AUTO_INCREMENT PRIMARY KEY,
NombreClase VARCHAR(60),
ID_Profesor INT,
FOREIGN KEY (ID_Profesor) REFERENCES Profesores(ID_Profesor)
);

CREATE TABLE Estudiantes_Clases (
ID_Estudiante INT,
ID_Clase INT,
PRIMARY KEY (ID_Estudiante, ID_Clase),
FOREIGN KEY (ID_Estudiante) REFERENCES Estudiantes(ID_Estudiante),
FOREIGN KEY (ID_Clase) REFERENCES Clases(ID_Clase)
);

SHOW TABLES;

INSERT INTO Estudiantes (Nombre, Apellido, Email) VALUES
('Juan', 'Pérez', 'juan.perez@gmail.com'),
('Ana', 'García', 'ana.garcia@gmail.com'),
('Carlos', 'Martínez', 'carlos.martinez@gmail.com'),
('Luisa', 'Fernández', 'luisa.fernandez@gmail.com');


INSERT INTO Profesores (Nombre, Apellido, Especialidad) VALUES
('Jose', 'Lopez', 'Matematicas'),
('Maria', 'Gonzalez', 'Historia'),
('Roberto', 'Alvarez', 'Ciencias'),
('Elena', 'Romero', 'Lengua');


INSERT INTO Clases (NombreClase, ID_Profesor) VALUES
('Algebra', 1),
('Historia del Arte', 2),
('Biología', 3),
('Literatura', 4);


INSERT INTO Estudiantes_Clases (ID_Estudiante, ID_Clase) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);


SELECT * FROM  Estudiantes;
SELECT * FROM  Profesores;
SELECT * FROM  Clases;
SELECT * FROM  Estudiantes_Clases;

